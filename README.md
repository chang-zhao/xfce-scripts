# xfce-scripts

Scripts useful with XFCE Desktop Environment. See also [chang-zhao/genmon-switch](/chang-zhao/genmon-switch) and [xfce4-genmon-plugin](https://docs.xfce.org/panel-plugins/xfce4-genmon-plugin/start).

## genmon-desktop.sh

This script can be used with XFCE Genmon plugin as a **custom replacement for "Show Desktop" button**, having arbitrary dimensions and image.

### Installation and settings

Set it as the command for Genmon plugin. Tick "Use a single panel row". Set maximal refresh period (86400), as there's no need to refresh it.

Requirements: installed `wmctrl`, `xorg-xprop`

Files:

```
scripts/genmon-desktop.sh
icons/showdesktop.png
```

Dimensions of `showdesktop.png` will define the size and shape of the button (e.g. 92x12 px). You can replace `showdesktop.png` with any other picture or edit it to your liking. If necessary, adjust margins with gtk.css as explained here:

[How can i edit show desktop panel plugin width?](https://forum.xfce.org/viewtopic.php?id=13869)

## thunar.sh

For a launcher button on an XFCE panel, you can set this script as the command:

```
</path/to>/thunar.sh %u
```

It has one advantage over standard Thunar launcher: if an instance of Thunar is already running, it's just brought to foreground. (The standard Thunar launcher would try to open more tabs, making a bit of a mess - if you use several tabs and restore them on launch).

Requirements: `wmctrl` installed.

## color_file.sh

Quickly mark files with colors

https://forum.xfce.org/viewtopic.php?id=17221

![look](icons/thunar-colored.webp)

![Custom Actions menu in Thunar](icons/thunar-color.webp)

## taskman-switch.sh

Set this as the command in the System Load Monitor plugin (clicking which launches the task manager).

The advantage of this script compared to just calling `xfce4-taskmanager` is that when the taskmanager is open, you can click the same System Load Monitor plugin and have the taskmanager closed (no need to look for the window close button). It's no big deal, but actually pretty convenient, especially if you need to run-and-close it often.

Requirements: installed `xfce4-taskmanager`.

## terminal.sh

Set this script as an XFCE Panel's Launcher command, like

```
   ~/scripts/terminal.sh %f
```

and then you could drop a directory at that launcher's button, and have the terminal opened in the directory.

## root_term.sh

Opens a terminal window as `root`. Otherwise, the same as above. Set this script as an XFCE Panel's Launcher command, like

```
~/scripts/root_term.sh %f
```

and then you could drop a directory at that launcher's button, and have the terminal opened in the directory.

Requirements: you might need to set authorization rules in `polkit` or something. See

https://wiki.archlinux.org/title/Polkit#Bypass_password_prompt

## terminal-cmd.sh

Run shell with arguments passed to this script and leave the terminal open.

This script can be useful in an XFCE Panel Launcher, and sometimes running from CLI.

## firejail.sh

Run command in firejail. (As usual, put it in Launcher; it can receive drag-drop).

## firelist.sh

Show the list of programs currently running in firejail.

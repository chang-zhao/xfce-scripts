#!/bin/sh
# Root terminal at dropped folder
# Set as a Launcher command like
#   ~/scripts/root_term.sh %f

{ [ -n "$1" ] && pkexec env DISPLAY=:0 XAUTHORITY=$HOME/.Xauthority xfce4-terminal --default-working-directory=$1 ; } || pkexec env DISPLAY=:0 XAUTHORITY=$HOME/.Xauthority xfce4-terminal

#!/bin/bash
# "Highlight" (color) files in Thunar
# Acknowledgments: ToZ (previous version)
# https://forum.xfce.org/viewtopic.php?id=17221

# $1 is a coloring code or "unset"

COLOR="$1"
case "$COLOR" in
   todo)    COL="rgb(255,230,200)" ;;
   later)   COL="rgb(220,220,220)" ;;
   unread)  COL="rgb(220,255,220)" ;;
esac

# shift to next set of parameters - the files to affect
shift

# if unset was passed, then remove all highlighting
if [ "$COLOR" == "unset" ]
then
   for DO_FILE in "$@"
   do
      gio set -t unset "$DO_FILE" metadata::thunar-highlight-color-background
   done

# otherwise highlight the files
else
   for DO_FILE in "$@"
   do
      gio set "$DO_FILE" metadata::thunar-highlight-color-background "$COL"
   done
fi

# refresh the thunar window
xdotool key F5
#!/bin/sh
# Terminal at dropped folder
# Set as a Launcher command like
#   ~/scripts/terminal.sh %f

{ [ -d "$1" ] && exo-open --working-directory "$1" --launch TerminalEmulator ; } || \
    exo-open --launch TerminalEmulator $@

#!/bin/sh
# Run command in firejail
FIREDIR=$(realpath "$1")
cd $(dirname "$FIREDIR")
/usr/bin/firejail "$@"

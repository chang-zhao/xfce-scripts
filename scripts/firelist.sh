#!/bin/sh
# Show the list of programs currently running in firejail
notify-send "Firejail" "$(firejail --list)"

#!/bin/sh
# This script can be used with XFCE Genmon plugin as a custom replacement
# for "Show Desktop" button, having arbitrary dimensions and image.
# Set it as the command for Genmon plugin. Tick "Use a single panel row".
# Set maximal refresh period (86400), as there's no need to refresh it.

# Requirements: `wmctrl` installed
# Files:
# ../scripts/genmon-desktop.sh
# ../icons/showdesktop.png

if [ "$1" == "clicked" ]; then
  if xprop -root _NET_SHOWING_DESKTOP| grep '= 1' ; then
    wmctrl -k off
  else
    wmctrl -k on
  fi
else
  SCRIPT_DIR=$(dirname "$0")
  echo "<img>${SCRIPT_DIR%/*}/icons/showdesktop.png</img>"
  echo "<tool>Show Desktop</tool>"
  echo "<click>$0 clicked</click>"
fi

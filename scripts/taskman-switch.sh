#!/bin/sh
# The advantage of this script compared to just `xfce4-taskmanager` is that
# you can click the same System Load plugin and have the taskmanager closed
# (no need to look for the window close button).
PROCID=`pgrep xfce4-taskman` && kill $PROCID || xfce4-taskmanager
